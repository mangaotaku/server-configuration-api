FROM node:7.10-alpine
ARG COMMIT_HASH
ARG NPM_API_KEY

# Create a folder for the app
RUN mkdir -p /app
WORKDIR /app

RUN apk add --update \
    python \
    python-dev \
    py-pip \
    build-base && \
    pip install awscli && \
    apk del py-pip build-base python-dev && \
    rm -rf /var/cache/apk/*

# Install dependencies
COPY package.json .
COPY tsconfig.json .
ENV NPM_API_KEY $NPM_API_KEY
RUN echo "//registry.npmjs.org/:_authToken=$NPM_API_KEY" > ~/.npmrc
RUN npm install

# Install the app
COPY src/ ./src
RUN echo $COMMIT_HASH && sed -i.bak s/UNKNOWN_HASH/${COMMIT_HASH}/g src/version.json

# Start the app
CMD ["npm", "start"]
