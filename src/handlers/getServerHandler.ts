import { IEvent, ErrorResponse } from 'configurapi';
import { JsonResponse } from 'configurapi-handler-json';
import ServerManager from '../managers/serverManager';

export async function getServerHandler(event: IEvent) {
    const id = event.params['server'];
    if(!id){
        event.response = new ErrorResponse(`Must have an id.`,404);
        return this.complete();
    }
    event.response = new JsonResponse(await new ServerManager().getOne(id));
}