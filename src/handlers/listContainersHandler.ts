import { ErrorResponse, IEvent } from 'configurapi';
import { JsonResponse } from 'configurapi-handler-json';

import DockerManager from '../managers/dockerManager';

export async function listContainersHandler(event: IEvent) {
    event.response = new JsonResponse(await new DockerManager().get());
}