import { ErrorResponse, IEvent } from 'configurapi';
import { JsonResponse } from 'configurapi-handler-json';

import DockerManager from '../managers/dockerManager';

export async function getContainerHandler(event: IEvent) {
    const id = event.params['container'];
    if(!id){
        event.response = new ErrorResponse(`Must have an id.`,404);
        return this.complete();
    }
    event.response = new JsonResponse(await new DockerManager().getOne(id));
}