import { IEvent } from 'configurapi';

import ServerManager from '../managers/serverManager';

export async function startServerHandler(event: IEvent) {
    const id = event.params['server'];
    await new ServerManager().start(id);
    event.response.statusCode = 204;
}