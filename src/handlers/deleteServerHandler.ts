import { IEvent } from 'configurapi';
import ServerManager from '../managers/serverManager';

export async function deleteServerHandler(event: IEvent) {
    await new ServerManager().delete(event.params['server']);
    event.response.statusCode = 204;
}