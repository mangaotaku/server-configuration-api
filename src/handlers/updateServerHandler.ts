import { IEvent } from 'configurapi';
import ServerConfiguration from '../entities/serverConfiguration';
import ServerManager from '../managers/serverManager';

export async function updateServerHandler(event: IEvent) {
    const serverId = event.params['server'];
    let originalServer = await new ServerManager().getOne(serverId);
    let updatedServer = {...event.payload};
    updatedServer.id = serverId;
    await new ServerManager().update(ServerConfiguration.fromAny({...originalServer, ...updatedServer}));
    event.response.statusCode = 204;
}