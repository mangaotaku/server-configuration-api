import { IEvent, Response } from 'configurapi';
import { JsonResponse } from 'configurapi-handler-json';
import ServerManager from '../managers/serverManager';
import ServerConfiguration from '../entities/serverConfiguration';

export async function createServerHandler(event: IEvent) {
    let server = ServerConfiguration.fromAny(event.payload);
    event.response = new JsonResponse({id: await new ServerManager().add(server)});
}