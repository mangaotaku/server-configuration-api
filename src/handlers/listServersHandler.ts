import { IEvent } from 'configurapi';
import { ListResponse } from 'configurapi-handler-json';
import ServerManager from '../managers/serverManager';

export async function listServersHandler(event: IEvent) {
    event.response = new ListResponse(await new ServerManager().get());
}