import { IEvent } from 'configurapi';

import ServerManager from '../managers/serverManager';

export async function stopServerHandler(event: IEvent) {
    const id = event.params['server'];
    await new ServerManager().stop(id);
    event.response.statusCode = 204;
}