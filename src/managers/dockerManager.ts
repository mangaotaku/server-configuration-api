import { Container, ContainerInfo } from 'dockerode';

import IDockerManager from '../interfaces/iDockerManager';
import IDockerRepository from '../interfaces/iDockerRepository';
import IServerConfiguration from '../interfaces/iServerConfiguration';
import DockerRepository from '../repositories/dockerRepository';

export default class DockerManager implements IDockerManager {
    constructor(private repository: IDockerRepository = new DockerRepository()){}

    async create(server: IServerConfiguration, availablePort: number): Promise<string> {
        return await this.repository.create(server, availablePort);
    }

    async update(): Promise<string> {
        return this.repository.update();
    }

    async start(container: Container): Promise<string> {
        return this.repository.start(container);
    }

    async stop(id: string): Promise<void> {
        return this.repository.stop(id);
    }

    async get(): Promise<ContainerInfo[]> {
        return this.repository.get();
    }

    async getOne(id: string): Promise<ContainerInfo> {
        return this.repository.getOne(id);
    }
}