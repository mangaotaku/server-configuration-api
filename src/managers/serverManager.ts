import IServerManager from "../interfaces/iServerManager";
import IServerConfiguration from "../interfaces/iServerConfiguration";
import IServerRepository from "../interfaces/iServerRepository";
import { v4 } from 'uuid';
import ServerRepository from "../repositories/serverRepository";
import IDockerManager from "../interfaces/iDockerManager";
import DockerManager from "./dockerManager";
import ServerStatus from "../interfaces/serverStatus";

export default class ServerManager implements IServerManager {
    repository: IServerRepository;
    dockerManager: IDockerManager;

    constructor(repository?: IServerRepository, dockerManager?: IDockerManager){
        this.repository = repository || new ServerRepository();
        this.dockerManager = dockerManager || new DockerManager();
    }

    async add(config: IServerConfiguration): Promise<string> {
        config.id = config.id || v4();
        await this.repository.add(config);
        return config.id;
    }

    async delete(id: string): Promise<void> {
        await this.repository.delete(id);
    }

    async start(id: string): Promise<void> {
        let containers = await this.dockerManager.get();
        let ports = new Set<number>();
        for(const container of containers){
            if(container.Ports){
                for(const port of container.Ports){
                    if(port.PublicPort){
                        ports.add(port.PublicPort);
                    }
                }
            }
        }
        let availablePort = 8000;
        for( let i = 8000; i <= 9000; i++ ){
            if(!ports.has(i)){
                availablePort = i;
                break;
            }
            if( i === 9000 ){
                throw new Error(`Exceeded 8000-9000 port range!`);
            }
        }

        const server = await this.getOne(id);
        await this.stop(server.id);
        await this.dockerManager.create(server, availablePort);
        return;
    }

    async updateImage(id: string): Promise<void> {
        const server = await this.getOne(id);
        let containers = await this.dockerManager.get();
        
        for( const container of containers ){
            if(container.Names.includes(`/${server.name}`)){
                await this.dockerManager.stop(container.Id);
            }
        }

        return;
    }

    async stop(id: string): Promise<void> {
        const server = await this.getOne(id);
        let containers = await this.dockerManager.get();
        
        for( const container of containers ){
            if(container.Names.includes(`/${server.name}`)){
                await this.dockerManager.stop(container.Id);
            }
        }
    }

    async update(config: IServerConfiguration): Promise<void> {
        await this.repository.update(config);
    }

    async get(): Promise<IServerConfiguration[]> {
        return await this.repository.get();
    }

    async getOne(id: string): Promise<IServerConfiguration> {
        let server = await this.repository.getOne(id);
        let containers = await this.dockerManager.get();
        
        for( const container of containers ){
            if(container.Names.includes(`/${server.name}`)){
                server.status = ServerStatus[container.State];
            }
        }
        return server;
    }

}