export default class Config {
    public static readonly MONGO_DB = 'mongodb';
    public static readonly TABLE_PREFIX = process.env.TABLE_PREFIX || 'mangaotaku';
    public static readonly AVAILABLE_DATABASE_PROVIDERS = [ Config.MONGO_DB ];
    public static readonly SERVER_CONFIGURATION_API_MONGO_DB_PATH = process.env.SERVER_CONFIGURATION_API_MONGO_DB_PATH || 'localhost:27017';
    public static readonly SERVER_CONFIGURATION_TABLE_NAME = `${Config.TABLE_PREFIX}-server-configuration`;
    public static readonly MONGO_DB_CONNECTION_STRING = `mongodb://${Config.SERVER_CONFIGURATION_API_MONGO_DB_PATH}/${Config.TABLE_PREFIX}`
    public static readonly DOCKER_PORT = process.env.DOCKER_PORT || 2376;
    public static readonly DOCKER_PATH = process.env.DOCKER_PATH || 'localhost';
    public static readonly DOCKER_PROTOCOL = process.env.DOCKER_PROTOCOL || 'http';
    public static readonly DOCKER_API_VERSION = process.env.DOCKER_API_VERSION || 'v1.24';
    public static readonly DEFAULT_EMAIL = "mgcmushroomz@gmail.com";
    public static readonly BASE_DOMAIN = 'mangaotaku.net';
    public static readonly DEFAULT_NETWORK = process.env.DEFAULT_NETWORK || 'nginx-proxy';
    public static readonly SOCKET_PATH = '/var/run/docker.sock';
}