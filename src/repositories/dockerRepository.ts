import { Container, ContainerInfo } from 'dockerode';
import IDockerRepository from "../interfaces/iDockerRepository";
import DockerProvider from "../providers/dockerProvider";
import IServerConfiguration from '../interfaces/iServerConfiguration';
import IDockerProvider from '../interfaces/iDockerProvider';

export default class DockerRepository implements IDockerRepository {
    provider: IDockerProvider;

    constructor(provider?: IDockerProvider){
        this.provider = provider || new DockerProvider();
    }

    async create(server: IServerConfiguration, availablePort: number): Promise<string> {
        return await this.provider.create(server, availablePort);
    }
    
    async stop(id: string): Promise<void> {
        return await this.provider.stop(id);
    }

    async update(): Promise<string> {
        return this.provider.update();
    }

    async start(container: Container): Promise<string> {
        return this.provider.start(container);
    }

    async get(): Promise<ContainerInfo[]> {
        return this.provider.get();
    }

    async getOne(id: string): Promise<ContainerInfo> {
        return this.provider.getOne(id);
    }
}