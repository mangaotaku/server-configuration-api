import Config from '../config';
import IServerConfiguration from '../interfaces/iServerConfiguration';
import IServerProvider from '../interfaces/iServerProvider';
import IServerRepository from '../interfaces/iServerRepository';
import ServerConfigurationProviderFactory from '../providers/serverConfigurationProviderFactory';

export default class ServerRepository implements IServerRepository {
    constructor(private provider: IServerProvider = ServerConfigurationProviderFactory.create(Config.MONGO_DB)){}

    async add(config: IServerConfiguration): Promise<void> {
        return await this.provider.add(config);
    }

    async delete(id: string): Promise<string> {
        return await this.provider.delete(id);
    }

    async update(config: IServerConfiguration): Promise<string> {
        return await this.provider.update(config);
    }

    async get(): Promise<IServerConfiguration[]> {
        return await this.provider.get();
    }

    async getOne(id: string): Promise<IServerConfiguration> {
        return await this.provider.getOne(id);
    }

}