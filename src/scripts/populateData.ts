import Config from '../config';
import ServerConfigurationProviderFactory from '../providers/serverConfigurationProviderFactory';

async function main() {
    let provider = await ServerConfigurationProviderFactory.create(Config.MONGO_DB);
    let servers = await provider.get();
    console.log("Found " + servers.length + " servers.")
    for(const server of servers){
        await provider.delete(server.id);
        console.log("removed " + server.id);
    }
    console.log("Finished populating data!")
}

main().then(()=>process.exit(0)).catch(e=>console.log(e));