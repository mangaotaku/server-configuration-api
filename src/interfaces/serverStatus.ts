enum ServerStatus {
    STARTING = 'starting',
    CREATED = 'created',
    RESTARTING = 'restarting',
    RUNNING = 'running',
    PAUSED = 'paused',
    EXITED = 'exited',
    DEAD = 'dead',
    UP = 'up',
    SHUTTING_DOWN = 'shutting_down',
    DOWN = 'down'
}

export default ServerStatus;