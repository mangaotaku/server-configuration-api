import { Container, ContainerInfo } from "dockerode";
import IServerConfiguration from "./iServerConfiguration";

export default interface IDockerManager {
    create(server: IServerConfiguration, availablePort: number): Promise<string>;
    update(): Promise<string>;
    start(container: Container): Promise<string>;
    get(): Promise<ContainerInfo[]>;
    getOne(id: string): Promise<ContainerInfo>;
    stop(id: string): Promise<void>;
}