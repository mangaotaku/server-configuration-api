import IServerConfiguration from "./iServerConfiguration";

export default interface IServerProvider {
    add(config: IServerConfiguration): Promise<void>;
    delete(id: string): Promise<string>;
    update(config: IServerConfiguration): Promise<string>;
    get(): Promise<IServerConfiguration[]>;
    getOne(id: string): Promise<IServerConfiguration>;
}