import IServerConfiguration from "./iServerConfiguration";

export default interface IServerManager {
    add(config: IServerConfiguration): Promise<string>;
    delete(id: string): Promise<void>;
    update(config: IServerConfiguration): Promise<void>;
    get(): Promise<IServerConfiguration[]>;
    getOne(id: string): Promise<IServerConfiguration>;
    start(id: string): Promise<void>;
    updateImage(id: string): Promise<void>;
    stop(id: string): Promise<void>;
}