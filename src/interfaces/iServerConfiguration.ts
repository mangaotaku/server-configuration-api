import ServerStatus from "./serverStatus";
import ServerType from "./serverType";

export default interface IServerConfiguration {
    id: string;
    name: string;
    port: number;
    description:string;
    image: string;
    status: ServerStatus;
    domain: string;
    public: boolean;
    type: ServerType;
    environment?: {[key: string]: string};
}