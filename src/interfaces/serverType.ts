enum ServerType {
    API = 'api',
    SERVICE = 'service'
}

export default ServerType;