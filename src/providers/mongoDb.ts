import monk from 'monk';
import Config from '../config';

export default monk(Config.MONGO_DB_CONNECTION_STRING);
