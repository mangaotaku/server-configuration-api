import Config from "../config";
import MongoDbServerConfigurationProvider from "./mongoDbServerConfigurationProvider";

export default class ServerConfigurationProviderFactory {
    static create(provider: string) {
        switch(provider){
            case Config.MONGO_DB:
                return new MongoDbServerConfigurationProvider();
            default:
                throw new Error(`Unknown provider '${provider}' requested, expected one of ${Config.AVAILABLE_DATABASE_PROVIDERS}`);
        }
    }    
}