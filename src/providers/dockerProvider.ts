import IDockerProvider from "../interfaces/iDockerProvider";
import * as Docker from 'dockerode';
import { Container, ContainerInfo } from 'dockerode';
import IServerConfiguration from "../interfaces/iServerConfiguration";
import Config from "../config";

export default class DockerProvider implements IDockerProvider {
    private client: Docker;

    constructor(){
        // this.client = new Docker({host: Config.DOCKER_PATH, port: Config.DOCKER_PORT, protocol: 'http', version: Config.DOCKER_API_VERSION});
        this.client = new Docker({socketPath: Config.SOCKET_PATH, version: Config.DOCKER_API_VERSION});
    }

    async create(server: IServerConfiguration, availablePort: number): Promise<string> {
        let environment = [
            `VIRTUAL_HOST=${server.domain}.${Config.BASE_DOMAIN}`,
            `LETSENCRYPT_HOST=${server.domain}.${Config.BASE_DOMAIN}`,
            `LETSENCRYPT_EMAIL=${Config.DEFAULT_EMAIL}`
        ];
        if(server.environment){
            for( const key of Object.keys(server.environment) ) {
                environment.push(`${key}=${server.environment[key]}`);
            }
        }
        await this.client.getImage(server.image);
        let container = await this.client.createContainer({
            Image: server.image,
            name: server.name,
            AttachStdin: false,
            AttachStdout: true,
            AttachStderr: true,
            Tty: true,
            HostConfig: {
                PortBindings: {
                    [`${server.port}/tcp`]:[{
                        HostIp: "127.0.0.1",
                        HostPort: `${availablePort}`
                    }]
                }
            },
            ExposedPorts: {
                [`${server.port}/tcp`]: {}
            },
            Env: environment
        });
        await container.start();
        const network = await this.client.getNetwork(Config.DEFAULT_NETWORK);
        await network.connect({ Container: container.id });
        return container.id;
    }
    
    async stop(id: string): Promise<void> {
        const container = await this.client.getContainer(id);
        await container.stop();
        await container.remove();
    }

    async update(): Promise<string> {
        let containers = await this.get();
        return;
    }

    async start(container: Container): Promise<string> {
        // return this.provider.start(container);
        return;
    }

    async get(): Promise<ContainerInfo[]> {
        return await this.client.listContainers();
    }

    async getOne(id: string): Promise<ContainerInfo> {
        let containers = await this.get();
        for(const container of containers){
            if(container.Id === id){
                return container;
            }
        }
        return undefined;
    }
}