import Config from '../config';
import IServerConfiguration from '../interfaces/iServerConfiguration';
import IServerProvider from '../interfaces/iServerProvider';
import db from './mongoDb';
import {ICollection} from 'monk';
import ServerConfiguration from '../entities/serverConfiguration';

export default class MongoDbServerConfigurationProvider implements IServerProvider {
    collections:ICollection<IServerConfiguration>;

    constructor(collections?:ICollection<any>)
    {
        this.collections = collections || db.get(Config.SERVER_CONFIGURATION_TABLE_NAME);
        this.collections.createIndex({ id: 1 }, { unique: true });
        this.collections.createIndex({ name: 1 });
        this.collections.createIndex({ port: 1 });
        this.collections.createIndex({ description: 1 });
        this.collections.createIndex({ image: 1 });
        this.collections.createIndex({ status: 1 });
        this.collections.createIndex({ domain: 1 });
        this.collections.createIndex({ public: 1 });
        this.collections.createIndex({ type: 1 });
        this.collections.createIndex({ environment: 1 });
    }


    async add(config: IServerConfiguration): Promise<void> {
        await this.collections.insert(config);
    }

    async delete(id: string): Promise<string> {
        await this.collections.remove({id: id});
        return id;
    }

    async update(config: IServerConfiguration): Promise<string> {
        await this.collections.update({id: config.id}, config);
        return;
    }

    async get(): Promise<IServerConfiguration[]> {
        let configs = await this.collections.find({});
        return configs.map(c => ServerConfiguration.fromAny(c));
    }

    async getOne(id: string): Promise<IServerConfiguration> {
        return ServerConfiguration.fromAny(await this.collections.findOne({id: id}));
    }
}