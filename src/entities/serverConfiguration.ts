import IServerConfiguration from "../interfaces/iServerConfiguration";
import ServerStatus from "../interfaces/serverStatus";
import ServerType from "../interfaces/serverType";

export default class ServerConfiguration implements IServerConfiguration {
    id: string;
    name: string;
    port: number;
    image: string;
    status: ServerStatus;
    description:string;
    domain: string;
    public: boolean;
    type: ServerType;
    environment: {[key: string]: any};

    constructor(config?: IServerConfiguration){
        this.id = config.id;
        this.name = config.name;
        this.image = config.image;
        this.port = config.port || 8080;
        this.status = config.status || ServerStatus.DOWN;
        this.description = config.description;
        this.domain = config.domain;
        this.public = config.public || false;
        this.type = config.type;
        this.environment = config.environment || undefined;
    }

    static fromAny(obj: any): ServerConfiguration {
        return new ServerConfiguration({...obj});
    }
}