import { assert } from 'chai';
import ServerConfiguration from '../src/entities/serverConfiguration';
import ServerStatus from '../src/interfaces/serverStatus';
import ServerType from '../src/interfaces/serverType';


describe('ServerConfiguration', () => {
    it('Serialization', async () => {
        const config = new ServerConfiguration({
            id: 'id', 
            name: 'name',
            port: 1234,
            image: 'image', 
            status: ServerStatus.DOWN, 
            description:'description',
            public: false,
            environment: {key: 'value'},
            domain: 'domain',
            type: ServerType.SERVICE
        });
        
        assert.equal(config.id, 'id');
        assert.equal(config.name, 'name');
        assert.equal(config.port, 1234);
        assert.equal(config.image, 'image');
        assert.equal(config.status, ServerStatus.DOWN);
        assert.equal(config.public, false);
        assert.deepEqual(config.environment, {key: 'value'});
        assert.equal(config.domain, 'domain');
        assert.equal(config.type, ServerType.SERVICE);
    });

    it('Serialization from JSON', () => {
        const json = { id:'id', name: 'name', port: 1234, image: 'image', anothervar: 'anothervar', status: ServerStatus.DOWN };
        const config = ServerConfiguration.fromAny(json);

        assert.equal(config.id, 'id');
        assert.equal(config.name, 'name');
        assert.equal(config.port, 1234);
        assert.equal(config.image, 'image');
        assert.isUndefined(config['anothervar']);
    });

});