#!/bin/sh

for i in `seq 1 36`; do

    echo "Wating for APIs..."
    
    nc -z mongodb 27017 && nc -z server-configuration 8080

    if [  "$?" -eq 0  ]; then
        break
    else
        sleep 5
    fi
done

nc -z mongodb 27017 && nc -z server-configuration 8080

if [ "$?" -eq 0 ]; then
    echo 'Services has started and ready.'
    exit 0
else
    echo 'Services has taken too long to start.  Gave up.'
    exit 1
fi