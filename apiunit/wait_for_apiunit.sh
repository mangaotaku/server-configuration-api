#!/bin/sh

for i in `seq 1 60`; do

    echo 'Checking for apiunit...'
    ls -l apiunit/apiunit.lo*

    if [ -f apiunit/apiunit.lock ]; then
        sleep 5
    else
        break
    fi
done

if [ -f apiunit/apiunit.log ]; then
    echo 'Test result:'
    ls -l apiunit/apiunit.log
    cat apiunit/apiunit.log
fi

if [ -f apiunit/apiunit.lock ]; then
    echo 'Testing has taken too long.'
    exit 1
fi

if grep -q 'Tests passed.' apiunit/apiunit.log ; then
    echo 'Tests passed.'
    exit 0
else
    echo 'Tests failed.'
    exit 1
fi
    
